#Database Control Version

##Config

```
> ├── README.md
> ├── build.xml
> ├── libs
> │   ├── flyway-ant-4.2.0.jar
> │   ├── flyway-core-4.2.0.jar
> │   ├── h2-1.4.191.jar
> │   └── mariadb-java-client-2.0.3.jar
> ├── migrations
> │   ├── V2__Create_person_table.sql
> │   ├── V3__alter_person_add_column.sql
> │   └── V4__alter_person_add_column2.sql
> ├── props
> │   └── LOCAL.db.properties
> └── schema_version
```


migrations folder: contains all the migrations, delthas. This value must be always incremental values V<numbe>__<description>
props folder: contains environments configurations
schema:_version file contains the based table to store the migrations, this table must exists in all schemas


##HOW IT WORKS

The main command to migrate the database to the latest version is, where param will be the Database where to apply the changes:

ant migrate-db -Dbuild-db=<param>